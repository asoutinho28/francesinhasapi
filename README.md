
# FrancesinhasApi (Pure Django)

The **_README.md_** file of this project is in [francesinhasApiProject folder](https://bitbucket.org/asoutinho28/francesinhasapi/src/master/francesinhasApiProject/)


# FrancesinhasApi (with Django REST Framework)

The **_README.md_** file of this project is in [francesinhasDrfProject folder](https://bitbucket.org/asoutinho28/francesinhasapi/src/master/francesinhasDrfProject/). It has a minor change in the service Post Review

