
# FrancesinhasApi (Pure Django)

The application is built using Django framework and written in Python. This project is based on pure Django without any REST framework, for example DRF (Django REST Framework).

The application is split into two models, which have their own services: _Restaurants_ and _Reviews_.

#### Restaurants Requests

Used to retrieve information about restaurants as well as the top **_N_** restaurants. 

#### Reviews 
Used to post a new review for a given restaurant.

## Prerequisites

* Docker Engine (Recommended version 19.03.5):
	* [Docker for Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
	* [Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/install/)
	* [Docker Destop for Mac](https://docs.docker.com/docker-for-mac/install/)
* [Docker Compose](https://docs.docker.com/compose/install/) (Recommended version 1.25.0)

#### For local setup of Project FrancesinhasDRF
* [Python3.5](https://www.python.org/downloads/release/python-350/) or [Python3.6](https://www.python.org/downloads/release/python-360/)
* [Django 2.2](https://docs.djangoproject.com/en/3.0/releases/2.2/)
* [Psycopg2-Binary 2.8.4](https://pypi.org/project/psycopg2-binary/)
* [Urllib3 1.13.1](https://pypi.org/project/urllib3/)
* [Requests 2.9.1](https://pypi.org/project/requests/2.9.1/)
* A Python IDE of your choosing, for example [PyCharm](https://www.jetbrains.com/pycharm/)


## Getting Started

Clone the repository into your preferred directory.


## Deployment

#### To execute the docker environment

**In your terminal, navigate to the directory of one of the projects available (FrancesinhasApi and FrancesinhasDrf) that contains the docker-compose.yaml file and execute the command:**

`docker-compose up` or `docker-compose up -d` for detached mode. (This may take some time the first time)

This will setup 2 docker containers, one running the API and the other running a PostgreSQL DB. 

During the API container setup, the code is copied into the container. The first commands to be executed are  **python manage.py makemigrations** and **python manage. py migrate** which will migrate the models of Django into the Postgres DB. After these migrations it will execute the same steps for the app project named "_api_" with the commands **python manage.py makemigrations api** and **python manage. py migrate api**. After both migrations are completed it will run the command **python manage.py runserver 0.0.0.0:8000**, in order to expose the API.

 * The container with the Django Api will expose the port **8000** to **1070** of your local machine
 * Postgres DB will expose the port **5432** to your local machine port **1060**

**IMPORTANT NOTE:** To successfully run the environment using docker, the postgres **_host_** and **_port_** in **francesinhas project settings.py** must be set to:

 * host - switch-postgres-db
 * port - 5432

#### To run using the IDE:

1. Edit the **_host_** and **_port_** property in **settings.py**:
	- If you have already performed `docker-compose up` you can use the dockerized Postgres DB instance. Just remember to set the properties _host_ and _port_ to **localhost** and **1060**.  
	
2. Import the Francesinhas project into your IDE.

3. Execute in terminal the same commands executed in the _docker compose_


## How to use ( FrancesinhasDRF API )

First you'll need to have an API Testing tool like Postman or Insomnia, in order to perform the exposed requests of the API. The main endpoint of the API is **_http://localhost:1070/api/restaurants/_**.
At first the Postgres DB will not have any restaurants stored, so if you go to the _url_ above it will perform a GET request to retrieve all restaurants and it will retrieve an **empty list**. In order to have restaurants, first you'll need to execute a POST request with **no body** to the endpoint **_api/restaurants/add_info/_**. 
 
 * **Restaurants - Restaurants Information and Update**
 	
	- GET Restaurants (http://localhost:1070/api/restaurants/)
		- Retrieves a list of restaurants with all of the information of _www.francesinhas.com_.
		- Ex: GET localhost:8080/flights?from=LIS&to=OPO&dateFrom=30/12/2019&dateTo=31/12/2019
	
	- GET Restaurant by Id (http://localhost:1070/api/restaurants/{id}/)
		- Returns a restaurant details of the given **_id_**.
		- Ex: GET http://localhost:1070/api/restaurants/1/

	- GET Top **_n_** restaurants (http://localhost:1070/api/restaurants/top/{n}/)
		- Returns the _top n_ restaurants of francesinhas.com stored in Postgres DB.
		- Ex: GET http://localhost:1070/api/restaurants/top/10/
		
	- PATCH Update restaurant (http://localhost:1070/api/restaurants/{id}/)
		- Updates the restaurant of the given id. The restaurant can be updated with all of the fields or just the ones the user wants.
		- Ex: PATCH http://localhost:1070/api/restaurants/{id}/
			- Fields of Json Body: 
				- phone_number - String;
				- city - String;
				- nr_reviews - Integer;
				- avg_rating - Decimal;
				- name - String;
				- photos_url - String;
				- url - String;
				- country - String;
				- reviews_url - String;
				- longitude - Decimal;
				- slug - String;
				- website - String;
				- email - String;
				- state - String;
				- postal_code - String;
				- profile_image_url - String;
				- marker_image_url - String;
				- address - String;
				- latitude - Decimal;
				- description - String.
				
 * **Restaurants - Add Restaurants from francesinhas.com**
 	
	- POST Add information of restaurants (http://localhost:1070/api/restaurants/add_info/)
		- Retrieves all restaurants in francesinhas.com and stores them in Postgres DB.
		- Ex: POST http://localhost:1070/api/restaurants/add_info/ No Body
		
* **Reviews - Post new Review of a Restaurant**
 	
	- POST Posts new review of a given Restaurant (http://localhost:1070/api/restaurants/add_info/)
		- Posts a new review to local DB and updates the number of reviews and average rating of the given restaurant based on the user review.
		- Ex: POST http://localhost:1070/api/restaurants/review/ 
			- Fields of Json Body: 
				- user - String;
				- comment - String;
				- rating - Integer;
				- restaurantId - Integer.


## Teardown the docker environment

To teardown the docker containers, images and volumes, simply go back to the directory containing the docker-compose.yaml file and execute `docker-compose down` followed by `docker system prune -a --volumes`.
This will delete all stopped containers, and images and volumes not currently being used so remember to watch out for any other docker resources you may need so they don't get removed down.
