import json
import logging
import sys

from django.db.utils import Error
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .external_api_handler import add_restaurants_info
from .models import Restaurant, Review
from .serializer import JsonModelSerializer

logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)
logger_format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(logger_format)
logger.addHandler(ch)
serializer = JsonModelSerializer()


def index(request):
    return JsonResponse({"message": "Hello I'm a pure Django Api !"}, status=200)


@csrf_exempt
def service_restaurant_by_id(request, restaurant_id):
    if (request.method == "PATCH") or (request.method == "PUT"):
        return update_restaurant(request, restaurant_id)
    elif request.method == "GET":
        return retrieve_restaurant_by_id(request, restaurant_id)
    else:
        return JsonResponse({"message": "%s method not allowed" % request.method}, status=405)


@csrf_exempt
def service_restaurants(request):
    if request.method == "GET":
        return retrieve_restaurants(request)
    elif request.method == "POST":
        return create_restaurant(request)
    else:
        return JsonResponse({"message": "%s method not allowed" % request.method}, status=405)


def retrieve_top_n_restaurants(request, top_n):
    if request.method != "GET":
        return JsonResponse({"message": "%s method not allowed" % request.method}, status=405)
    try:
        restaurants_obj = Restaurant.objects.order_by('-avg_rating')[:int(top_n)].values()
    except Restaurant.DoesNotExist:
        logger.error("Error there are no restaurants")
        return JsonResponse({"message": "There are no restaurants available at the moment."}, status=404)
    return JsonResponse(list(restaurants_obj), safe=False, status=200)


@csrf_exempt
def add_restaurants(request):
    if request.method != "POST":
        return JsonResponse({"message": "%s method not allowed" % request.method}, status=405)
    try:
        add_restaurants_info()
    except Exception as e:
        logger.error("Error adding restaurants info: %s" % str(e))
        return JsonResponse({"message": "Could not add restaurants from francesinhas.com"}, status=500)
    return JsonResponse({"message": "Restaurants have been added to db"}, safe=False, status=201)


@csrf_exempt
def create_review(request):
    if request.method != "POST":
        return JsonResponse({"message": "%s method not allowed" % request.method}, status=405)
    try:
        review_to_create = json.loads(request.body.decode('utf-8'))
        rating = review_to_create['rating']
        if (int(rating) < 1) or (int(rating) > 5):
            return JsonResponse({"message": "Please rate the review between 1 and 5."}, status=400)
        restaurant_id = review_to_create['restaurantId']
        restaurant = Restaurant.objects.get(pk=restaurant_id)
        Review.objects.create(user=review_to_create['user'], comment=review_to_create['comment'],
                              rating=review_to_create['rating'],
                              restaurant=restaurant)
        logger.info("Created new review for restaurant with id %s" % review_to_create['restaurantId'])
        update_avg_rating(review_to_create, restaurant)
        logger.info("Updated average rating with new review")
    except Error as e:
        logger.error(
            "Error could not post review of restaurant with id %s: " % review_to_create['restaurantId'] + str(e))
        return JsonResponse({"message": "Could not post review of restaurant %s." % review_to_create["restaurantId"]},
                            status=500)
    except KeyError as keyEx:
        return JsonResponse({"message": "Error parsing field %s" % str(keyEx)})
    return JsonResponse(review_to_create, safe=False, status=201)


def retrieve_restaurants(request):
    try:
        restaurants = Restaurant.objects.all()
        restaurants_serialized = serializer.serialize(restaurants)
        restaurant_json = json.loads(restaurants_serialized)
    except Restaurant.DoesNotExist:
        logger.error("Error there are no restaurants.")
        return JsonResponse({"message": "There are no restaurants available at the moment."}, status=404)
    return JsonResponse(list(restaurant_json), safe=False, status=200)


def create_restaurant(request):
    try:
        restaurant_to_create = json.loads(request.body.decode('utf-8'))
        Restaurant.objects.create(**restaurant_to_create)
    except Error as e:
        logger.error("Error could not create restaurant %s: " % restaurant_to_create['name'] + str(e))
        return JsonResponse({"message": "Could not create restaurant %s." % restaurant_to_create['name']}, status=201)
    return JsonResponse(restaurant_to_create, safe=False, status=201)


def retrieve_restaurant_by_id(request, restaurant_id):
    try:
        restaurant = Restaurant.objects.get(pk=restaurant_id)
        restaurant_serialized = serializer.serialize([restaurant, ])
        restaurant_json = json.loads(restaurant_serialized)
    except Restaurant.DoesNotExist:
        logger.error("Error could find restaurant with id %s: " % restaurant_id)
        return JsonResponse({"message": "There is no restaurant with id %s." % restaurant_id}, status=404)
    return JsonResponse(restaurant_json, safe=False, status=200)


def update_restaurant(request, restaurant_id):
    try:
        restaurant_to_update = json.loads(request.body.decode('utf-8'))
        Restaurant.objects.filter(id=restaurant_id).update(**restaurant_to_update)
        logger.info("Updated restaurant with id %s" % restaurant_id)
    except Error as e:
        logger.error("Error could not update restaurant with id %s: " % restaurant_id + str(e))
        return JsonResponse({"message": "Could not update restaurant with id %s" % restaurant_id}, status=500)
    return JsonResponse(restaurant_to_update, status=200)


def update_avg_rating(request_post, restaurant_for_review):
    logger.info("Calculate new average rating of the given restaurant")
    avg_rat_rest = restaurant_for_review.avg_rating
    nr_reviews_rest = restaurant_for_review.nr_reviews
    sum_ratings = avg_rat_rest * nr_reviews_rest
    sum_ratings += request_post['rating']
    new_nr_reviews = nr_reviews_rest + 1
    new_avg_rating = sum_ratings / (nr_reviews_rest + 1)
    logger.info("Update average rating and add one more review to number of reviews of the given restaurant")
    try:
        Restaurant.objects.filter(id=restaurant_for_review.id).update(avg_rating=new_avg_rating,
                                                                      nr_reviews=new_nr_reviews)
    except Error as e:
        logger.error("Error could not update restaurant with id %s: " % restaurant_for_review.id + str(e))
        return JsonResponse({"message": "Could not update restaurant with id %s" % restaurant_for_review.id},
                            status=500)
