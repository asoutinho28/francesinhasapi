from django.urls import path

from . import views

urlpatterns = [
    path('index/', views.index, name='index'),
    path('<int:restaurant_id>/', views.service_restaurant_by_id, name='service_restaurant_by_id'),
    path('add_info/', views.add_restaurants, name='add_restaurants_info'),
    path('', views.service_restaurants, name='service_restaurants'),
    path('top/<int:top_n>', views.retrieve_top_n_restaurants, name='top_n_restaurants'),
    path('review/', views.create_review, name='create_review'),
]
