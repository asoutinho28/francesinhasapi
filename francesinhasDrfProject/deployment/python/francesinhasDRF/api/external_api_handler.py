import json
import logging
import sys

import requests
from api.jsonModels import json_serializer
from django.core import serializers

logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)
logger_format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(logger_format)
logger.addHandler(ch)


def add_restaurants_info():
    response = requests.get('http://francesinhas.com/api/v1/places?per_page=100')

    j_obj = json.loads(response.text)
    j_serializer = json.loads(json_serializer)
    parse_and_save(j_obj, j_serializer)
    logger.info("Save first batch of restaurants")
    total_pages = j_obj['pagination']['total_pages']
    if total_pages > 1:
        logger.info("Proceed with retrieving the rest of the restaurants")
        for i in range(1, total_pages + 1):
            response = requests.get('http://francesinhas.com/api/v1/places?per_page=100&page=' + str(i))
            j_obj = json.loads(response.text)
            parse_and_save(j_obj, j_serializer)


def parse_and_save(j_obj, j_serializer):
    for coll in j_obj.get("collection", []):

        j_serializer[0]['fields']['phone_number'] = coll['phone_number']
        j_serializer[0]['fields']['city'] = coll['city']
        j_serializer[0]['fields']['avg_rating'] = coll['rating']['average']
        j_serializer[0]['fields']['nr_reviews'] = coll['rating']['reviews']
        j_serializer[0]['fields']['name'] = coll['name']
        j_serializer[0]['fields']['photos_url'] = coll['photos']['url']
        j_serializer[0]['fields']['url'] = coll['url']
        j_serializer[0]['fields']['country'] = coll['country']
        j_serializer[0]['fields']['reviews_url'] = coll['reviews']['url']
        j_serializer[0]['fields']['longitude'] = coll['longitude']
        j_serializer[0]['fields']['slug'] = coll['slug']
        j_serializer[0]['fields']['website'] = coll['website']
        j_serializer[0]['fields']['email'] = coll['email']
        j_serializer[0]['fields']['state'] = coll['state']
        j_serializer[0]['fields']['postal_code'] = coll['postal_code']
        j_serializer[0]['fields']['profile_image_url'] = coll['profile_image_url']
        j_serializer[0]['fields']['marker_image_url'] = coll['marker_image_url']
        j_serializer[0]['fields']['address'] = coll['address']
        j_serializer[0]['fields']['latitude'] = coll['latitude']
        j_serializer[0]['fields']['id'] = coll['id']
        j_serializer[0]['fields']['description'] = coll['description']

        for deserialized_object in serializers.deserialize("json", json.dumps(j_serializer)):
            deserialized_object.save()


class ExternalApiHandler:
    pass
