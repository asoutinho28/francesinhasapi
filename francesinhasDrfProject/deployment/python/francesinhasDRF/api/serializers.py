from rest_framework import serializers
from .models import Restaurants, Reviews


class RestaurantsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurants
        fields = '__all__'
        read_only_fields = ['id']

    def validate_name(self, value):
        qs = Restaurants.objects.filter(name__iexact=value)
        if qs.exists():
            raise serializers.ValidationError("This name has already been used")
        return value


class ReviewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reviews
        fields = '__all__'
        read_only_fields = ['id']

