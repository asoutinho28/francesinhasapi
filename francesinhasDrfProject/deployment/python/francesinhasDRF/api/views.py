import json
import logging
import sys

from django.db import Error
from django.template import RequestContext
from django.template.context_processors import request
from rest_framework.response import Response
from rest_framework import status, viewsets
from rest_framework import generics, mixins
from .models import Restaurants, Reviews
from .external_api_handler import add_restaurants_info
from .serializers import RestaurantsSerializer, ReviewsSerializer
from django.http import JsonResponse

logger = logging.getLogger('')
logger.setLevel(logging.DEBUG)
logger_format = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(logger_format)
logger.addHandler(ch)


class AddRestaurantsView(mixins.CreateModelMixin, generics.ListAPIView):
    pass
    serializer_class = RestaurantsSerializer
    context_instance = RequestContext(request)

    def post(self, request_post, *args, **kwargs):
        try:
            add_restaurants_info()
        except Exception as e:
            logger.error("Error adding restaurants info: %s" % str(e))
            return JsonResponse({"message": "Could not add restaurants from francesinhas.com"},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return JsonResponse({"message": "Restaurants have been added to db"}, status=status.HTTP_201_CREATED)


class RestaurantsAPIView(mixins.CreateModelMixin, generics.ListAPIView, mixins.UpdateModelMixin):
    pass
    serializer_class = RestaurantsSerializer
    context_instance = RequestContext(request)

    def get_queryset(self):
        return Restaurants.objects.all()

    def post(self, request_post, *args, **kwargs):
        bulk = isinstance(request_post.data, list)

        if not bulk:
            logger.info("Perform creation of only one restaurant")
            return self.create(request_post, *args, **kwargs)

        else:
            logger.info("Serialize list of restaurants to perform bulk create")
            serializer = self.get_serializer(data=request_post.data, many=True)
            serializer.is_valid(raise_exception=True)
            logger.info("Perform bulk creation of many restaurants")
            self.perform_bulk_create(serializer)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    # Creation of a given serializer
    def perform_bulk_create(self, serializer):
        return self.perform_create(serializer)

    def update(self, request_patch, *args, **kwargs):
        return Restaurants.objects.filter(id=request_patch.data['id']).update(**request_patch)


class RestaurantsView(generics.RetrieveUpdateDestroyAPIView):
    pass
    lookup_field = 'id'  # url(r'(?P<id>\d+)')
    serializer_class = RestaurantsSerializer

    def get_queryset(self):
        return Restaurants.objects.all()

    def update(self, request_patch, *args, **kwargs):
        update_obj = json.loads(request_patch.body.decode('utf-8'))
        try:
            Restaurants.objects.filter(id=kwargs.get('id')).update(**update_obj)
            logger.info("Updated restaurant with id %s" % kwargs.get('id'))
        except Error as e:
            logger.error("Error could not update restaurant with id %s: " % kwargs.get('id') + str(e))
            return JsonResponse({"message": "Could not update restaurant with id %s" % kwargs.get('id')},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return JsonResponse(request_patch.data, status=status.HTTP_200_OK)


class TopNRestaurants(viewsets.ModelViewSet):
    serializer_class = RestaurantsSerializer

    def list(self, request_get,top_n):
        try:
            restaurants_obj = Restaurants.objects.order_by('-avg_rating')[:int(top_n)].values()
        except Restaurants.DoesNotExist:
            logger.error("Error there are no restaurants")
            return JsonResponse({"message": "There are no restaurants available at the moment."},
                                status=status.HTTP_404_NOT_FOUND)
        return JsonResponse({"restaurants": list(restaurants_obj)})


class PostRestaurantReview(mixins.CreateModelMixin, generics.ListAPIView):
    lookup_field = 'id'  # url(r'(?P<id>\d+)') regex pattern for url endpoint to find by ID
    serializer_class = ReviewsSerializer
    context_instance = RequestContext(request)

    def get_queryset(self):
        return Reviews.objects.all()

    def post(self, request_post, *args, **kwargs):
        rating = request_post.data['rating']
        if (int(rating) < 1) or (int(rating) > 5):
            return JsonResponse({"message": "Please rate the review between 1 and 5."}, status=400)
        self.update_avg_rating(request_post)
        return self.create(request_post, *args, **kwargs)

    def update_avg_rating(self, request_post):
        logger.info("Calculate new average rating of the given restaurant")
        restaurant_for_review = Restaurants.objects.get(id=request_post.data['restaurant'])
        avg_rat_rest = restaurant_for_review.avg_rating
        nr_reviews_rest = restaurant_for_review.nr_reviews
        sum_ratings = avg_rat_rest * nr_reviews_rest
        sum_ratings += request_post.data['rating']
        new_nr_reviews = nr_reviews_rest + 1
        new_avg_rating = sum_ratings / new_nr_reviews
        logger.info("Update average rating and add one more review to number of reviews of the given restaurant")
        try:
            Restaurants.objects.filter(id=restaurant_for_review.id).update(avg_rating=new_avg_rating,
                                                                           nr_reviews=new_nr_reviews)
        except Error as e:
            logger.error("Error could not update restaurant with id %s: " % restaurant_for_review.id + str(e))
            return JsonResponse({"message": "Could not update restaurant with id %s" % restaurant_for_review.id},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
