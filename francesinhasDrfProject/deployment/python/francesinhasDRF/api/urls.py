from django.conf.urls import url
from .views import RestaurantsView, RestaurantsAPIView, AddRestaurantsView, TopNRestaurants, PostRestaurantReview

urlpatterns = [
    url(r'^$', RestaurantsAPIView.as_view(), name='post-restaurants'),
    url(r'^(?P<id>\d+)/$', RestaurantsView.as_view(), name='get-restaurants-by-id'),
    url(r'^add_info/$', AddRestaurantsView.as_view(), name='add-restaurants'),
    url(r'^top/(?P<top_n>\d+)/$', TopNRestaurants.as_view({'get': 'list'}), name='get-top-n-restaurants'),
    url(r'^review/$', PostRestaurantReview.as_view(), name='post-review'),
]

