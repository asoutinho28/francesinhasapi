from django.db import models


class Restaurants(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100, default='')
    avg_rating = models.DecimalField(max_digits=15, decimal_places=6, default=0)
    phone_number = models.CharField(max_length=100, default='')
    city = models.CharField(max_length=100, default='')
    nr_reviews = models.DecimalField(max_digits=15, decimal_places=6, default=0)
    url = models.CharField(max_length=100, default='')
    country = models.CharField(max_length=100, default='')
    photos_url = models.CharField(max_length=100, default='')
    longitude = models.DecimalField(max_digits=15, decimal_places=6, default=0)
    slug = models.CharField(max_length=100, default='')
    website = models.CharField(max_length=100, default='')
    reviews_url = models.CharField(max_length=100, default='')
    email = models.CharField(max_length=100, default='')
    state = models.CharField(max_length=100, default='')
    postal_code = models.CharField(max_length=100, default='')
    profile_image_url = models.CharField(max_length=100, default='')
    marker_image_url = models.CharField(max_length=100, default='')
    address = models.CharField(max_length=100, default='')
    latitude = models.DecimalField(max_digits=15, decimal_places=6, default=0)
    description = models.CharField(max_length=100, default='')

    def __str__(self):
        return self.name


class Reviews(models.Model):
    user = models.CharField(max_length=100, default='')
    comment = models.CharField(max_length=500, default='')
    rating = models.IntegerField()
    restaurant = models.ForeignKey(Restaurants, on_delete=models.CASCADE)

    def __str__(self):
        return self.comment
